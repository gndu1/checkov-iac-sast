from aws_cdk import core, aws_s3 as s3
from aws_cdk.core import RemovalPolicy

class S3Code(core.Stack):
    def __init__(self, app: core.App, id: str) -> None:
        super().__init__(app, id)

        # Violate CKV_AWS_21
        bucket = s3.Bucket(self,
            'bridgecrew-cdk-bucket',
            bucket_name='bridgecrew-cdk-bucket',
            removal_policy=RemovalPolicy.DESTROY)


app = core.App()
S3Code(app, "S3CodeExample")
app.synth()